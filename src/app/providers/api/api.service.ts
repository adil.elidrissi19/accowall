import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP_CONFIG } from './../../app.config';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    @Inject(APP_CONFIG) private config: any,
    private http: HttpClient
  ) { }

  register(user: any) {
    //return this.http.post(`${this.config.protocol}${this.config.host}${this.config.api_version}register`, user);
    let path = `${this.config.protocol}${this.config.host}${this.config.api_version}register`;
    let code = parseInt(user['code']);
    if(code >= 1000 && code <= 1100) path += '/no';
    return this.http.post(path, user);
  }
}
